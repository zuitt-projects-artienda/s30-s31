//express should be import because we will use route method
const express = require('express');

// allow us to access our HTTP methods routes
const router = express.Router();

const userControllers = require('../controllers/userControllers')

router.post('/', userControllers.createUserController);

router.get('/', userControllers.getAllUsersController);

router.put('/updateUserUsername/:id', userControllers.updateUserUsernameController)

router.get('/getSingleUser/:id', userControllers.getSingleUserController)



module.exports = router;