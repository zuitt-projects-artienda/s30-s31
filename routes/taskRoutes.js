//express should be import because we will use route method
const express = require('express');

// allow us to access our HTTP methods routes
const router = express.Router();

const taskControllers = require('../controllers/taskControllers')

//create a task route
router.post('/', taskControllers.createTaskController);

//retrieving all task route
router.get('/', taskControllers.getAllTasksController);

//retrieving single task
router.get('/getSingleTask/:id', taskControllers.getSingleTasksController)

//update
router.put('/updateTaskStatus/:id', taskControllers.updateTaskStatusController)

module.exports = router;