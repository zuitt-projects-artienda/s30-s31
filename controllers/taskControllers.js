const Task = require('../models/Task');

module.exports.createTaskController =(req, res) => {

	console.log(req.body);

	Task.findOne({name: req.body.name}).then (result => {
		if(result !== null && result.name === req.body.name){
			return res.send('Duplicate task found')
		} else {
			let newTask = new Task ({
				name: req.body.name,
				status: req.body.status
			})
			newTask.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));
		}
	})
	//promises are basically values that are unknown now, but will get a value later, and .then() and .catch() are like if-else statements to that unknown value, .then() will have code that handles the result of that unknown value, and .catch() is for handling errors involving that unknown value
	.catch(error => res.send(error));
};

module.exports.getAllTasksController =(req, res) => {
	//db.tasks.find({})
	Task.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};

module.exports.getSingleTasksController =(req, res) => {
	
	console.log(req.params);

	Task.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))

};

module.exports.updateTaskStatusController = (req, res) => {

	console.log(req.params.id);
	console.log(req.body);

	let updates = {
		status : req.body.status
	}

	//should have 3 arguements
	//{new: true} - allows us to return the updated version of the document we were updating, by default, without this, findByIdAndUpdate, will return the previous state of the documents or previous version.
	Task.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};





