// Note: all packages/modules should be required at the top of the file to avoid tampering or errors.
const express = require('express');

//a package that uses an ODM(Object document mapper). It allows us to translate our JS object into database documents for MongoDB. Allows connection and easier manipulation of our documents in MongoDB.
const mongoose = require('mongoose');

const taskRoutes = require('./routes/taskRoutes')
const userRoutes = require('./routes/userRoutes')

const app = express();

const port = 4000;

/*
	Syntax:
		mongoose.connect("<connectionStringFromMongoDB>", {
			useNewUrlParser: true,
			useUnifiedTopology: true
		})
*/

//enter your <password>
//changed MyFirstDatabase into task169
mongoose.connect("mongodb+srv://admin_artienda:admin169@cluster0.mtvrk.mongodb.net/task169?retryWrites=true&w=majority",{
		useNewUrlParser: true,
		useUnifiedTopology: true
});
// Create notifications if the connection to the db is successful or not.
let db = mongoose.connection;
//when db has connection error both in the  terminal and browser.
//EVENTS 
db.on('error', console.error.bind(console, "Connection Error"));

//once connection is connected/successful
db.once('open', () => console.log("Connected to MongoDB"));

app.use(express.json());

//our server will use a middelware to group all task routes under /tasks.
// All the endpoints in taskRoutes file will start with /tasks.
app.use('/tasks', taskRoutes);
app.use('/users', userRoutes);


app.listen(port, () => console.log(`Server is running at port ${port}`))